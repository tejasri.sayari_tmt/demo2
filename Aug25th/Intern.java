package Aug25th;

class Intern extends DataProvider {
    int marksSecured;
    int graceMarks;


    Intern(int marksSecured, int graceMarks) {
        this.marksSecured = marksSecured;
        this.graceMarks = graceMarks;
    }
    @Override
    public void calcPercentage() {
        float percent = ((float) marksSecured + (float)graceMarks) / (float) totalMaximumMarks*100;
        System.out.println(percent);
    }
}


